package by.htp.library;

import by.htp.library.controller.Controller;

import by.htp.library.view.PrintResponse;
import org.apache.log4j.Logger;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import static by.htp.library.util.constant.SpringConstant.CONFIGURATION_FILE_SPRING;
import static by.htp.library.util.constant.StartPointMessage.DESTROY_SOURCE;
import static by.htp.library.util.constant.StartPointMessage.INITIALIZATION_SOURCE;


public class StartPoint {

    private static final Logger log = Logger.getLogger(StartPoint.class);

    public static void main(String[] args) {

        String response;

//		##Initialization DB connection##


        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(CONFIGURATION_FILE_SPRING);

        Controller CONTROLLER = context.getBean("controller", Controller.class);

        response = CONTROLLER.executeAction("INITIALIZATION ");
        log.debug(INITIALIZATION_SOURCE);
        PrintResponse.out(response);


//		##Sign up user
        response = CONTROLLER.executeAction("sign_up Василий_Пупкин 12345678");
        PrintResponse.out(response);


        //##Sign in user
        response = CONTROLLER.executeAction("sign_in Василий_Пупкин 12345678");
        PrintResponse.out(response);


// 		##Add new book
//		Example: add_new_book Title Genre Author Year Quantity
        response = CONTROLLER.executeAction("add_new_book MyBook Action Vasya_Pupkin 2017 2");
        PrintResponse.out(response);


// 		##Add edit book
//		Example: add_edit_book Title Genre Author Year Quantity idBook
        response = CONTROLLER.executeAction("add_edit_book MyBook Action Petya_Pupkin 2017 2 15");
        PrintResponse.out(response);


//		##Get booklist
        CONTROLLER.executeAction("get_booklist ");
        PrintResponse.out(response);



// 		##Remove book
//		Example: remove_book idBook
//        response = CONTROLLER.executeAction("remove_book 10");
//        PrintResponse.out(response);


//		##Destroy DB connection##
        response = CONTROLLER.executeAction("DESTROY SOURCE ");
        log.debug(DESTROY_SOURCE);
        PrintResponse.out(response);
    }
}
