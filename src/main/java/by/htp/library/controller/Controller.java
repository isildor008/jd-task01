package by.htp.library.controller;

import by.htp.library.controller.command.CommandProvider;
import by.htp.library.controller.command.Command;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Controller
public final class Controller {
    private final String paramDelimeter = " ";
    private final int START_READ_REQUEST = 0;

    @Autowired
    private CommandProvider provider;

    public String executeAction(String request) {
        String commandName;
        Command command;

        commandName = request.substring(START_READ_REQUEST, request.indexOf(paramDelimeter));

        command = provider.getCommand(commandName);

        String response = command.executeCommand(request);

        return response;
    }
}
