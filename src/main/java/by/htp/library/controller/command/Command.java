package by.htp.library.controller.command;

public interface Command {
    String executeCommand(String request);
}
