package by.htp.library.controller.command;

public enum CommandName {

    INITIALIZATION,
    DESTROY_SOURCE,

    ADD_NEW_BOOK,
    SIGN_IN,
    SIGN_UP,
    GET_BOOK_LIST,
    ADD_EDIT_BOOK,

    WRONG_REQUEST
}
