package by.htp.library.controller.command;

import java.util.HashMap;
import java.util.Map;

import by.htp.library.controller.command.impl.AddEditBook;
import by.htp.library.controller.command.impl.AddNewBook;
import by.htp.library.controller.command.impl.DestroySource;
import by.htp.library.controller.command.impl.GetBookList;
import by.htp.library.controller.command.impl.SignIn;
import by.htp.library.controller.command.impl.SignUp;
import by.htp.library.controller.command.impl.InitializationSource;
import by.htp.library.controller.command.impl.WrongRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class CommandProvider {
    private static final Logger log = Logger.getLogger(CommandProvider.class);
    private static final Map<CommandName, Command> repository = new HashMap<>();

    @Autowired
    InitializationSource initializationSource;

    @Autowired
    DestroySource destroySource;

    @Autowired
    AddNewBook addNewBook;

    @Autowired
    SignIn signIn;

    @Autowired
    SignUp signUp;

    @Autowired
    AddEditBook addEditBook;

    @Autowired
    GetBookList getBookList;

    @Autowired
    WrongRequest wrongRequest;

    public Command getCommand(String key) {
        Command command;
        CommandName commandName;
        repository.put(CommandName.INITIALIZATION, initializationSource);
        repository.put(CommandName.DESTROY_SOURCE, destroySource);
        repository.put(CommandName.ADD_NEW_BOOK, addNewBook);
        repository.put(CommandName.SIGN_IN, signIn);
        repository.put(CommandName.SIGN_UP, signUp);
        repository.put(CommandName.ADD_EDIT_BOOK, addEditBook);
        repository.put(CommandName.GET_BOOK_LIST, getBookList);
        repository.put(CommandName.WRONG_REQUEST, wrongRequest);

        try {
            commandName = CommandName.valueOf(key.toUpperCase());
            command = repository.get(commandName);
            log.debug(command);
        } catch (IllegalArgumentException | NullPointerException e) {
            log.error(e);
            command = repository.get(CommandName.WRONG_REQUEST);
        }

        return command;
    }

}
