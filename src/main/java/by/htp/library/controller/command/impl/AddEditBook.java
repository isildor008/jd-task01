package by.htp.library.controller.command.impl;

import by.htp.library.controller.command.Command;
import by.htp.library.service.BookService;
import by.htp.library.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static by.htp.library.util.constant.CommandConstant.*;

@Component

public class AddEditBook implements Command {

    private static final Logger log = Logger.getLogger(AddEditBook.class);

    @Autowired
    BookService bookService;

    @Override
    public String executeCommand(String request) {
        String[] parameter = request.split(RESPONSE_SPLITER);
        String title = parameter[1];
        String author = parameter[2];
        String genre = parameter[3];
        String year = parameter[4];
        String quantity = parameter[5];
        String idBook = parameter[6];

        String response;

        try {
            bookService.addEditBook(title, genre, author, year, quantity, idBook);
            response = SUCCESSFUL_MESSAGE_FOR_EDIT_BOOK;
            log.debug(SUCCESSFUL_MESSAGE_FOR_EDIT_BOOK);
        } catch (ServiceException e) {
            response = ERROR_EDIT_FOR_BOOK;
            log.error(e);

        }
        return response;
    }

}
