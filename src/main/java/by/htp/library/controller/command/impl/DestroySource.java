package by.htp.library.controller.command.impl;

import by.htp.library.controller.command.Command;
import by.htp.library.service.InitializationService;
import by.htp.library.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static by.htp.library.util.constant.CommandConstant.ERROR_FOR_DATABASE_DESTROY;
import static by.htp.library.util.constant.CommandConstant.SUCCESSFUL_MESSAGE_FOR_DESTROY_DATABASE;

@Component
public class DestroySource implements Command {

    @Autowired
    InitializationService initializationService;

    private static final Logger log = Logger.getLogger(DestroySource.class);

    @Override

    public String executeCommand(String request) {

        String response;

        try {
            initializationService.destroy();
            response = SUCCESSFUL_MESSAGE_FOR_DESTROY_DATABASE;
            log.debug(SUCCESSFUL_MESSAGE_FOR_DESTROY_DATABASE);
        } catch (ServiceException e) {
            response = ERROR_FOR_DATABASE_DESTROY;
            log.error(e);
        }

        return response;
    }

}
