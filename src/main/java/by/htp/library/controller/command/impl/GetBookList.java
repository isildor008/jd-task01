package by.htp.library.controller.command.impl;

import java.util.List;

import by.htp.library.entuty.Book;
import by.htp.library.controller.command.Command;
import by.htp.library.service.BookService;
import by.htp.library.service.exception.ServiceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static by.htp.library.util.constant.CommandConstant.ERROR_FOR_BOOK_LIST;
import static by.htp.library.util.constant.CommandConstant.SUCCESSFUL_MESSAGE_FOR_BOOK_LIST;
@Component
public class GetBookList implements Command {

    @Autowired
    BookService bookService;
    private static final Logger log = Logger.getLogger(GetBookList.class);

    @Override
    public String executeCommand(String request) {


        List<Book> bookList;
            String response;
        try {
            bookList = bookService.getBookList();
            response = SUCCESSFUL_MESSAGE_FOR_BOOK_LIST;

            //Circle just for test
            for (Book book : bookList) {
                response = book.toString();
                log.debug(SUCCESSFUL_MESSAGE_FOR_BOOK_LIST);
            }
        } catch (ServiceException e) {
            response = ERROR_FOR_BOOK_LIST;
            log.error(e);
        }

        return response;
    }

}
