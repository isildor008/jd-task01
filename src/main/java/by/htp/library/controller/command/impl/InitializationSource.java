package by.htp.library.controller.command.impl;
import by.htp.library.controller.command.Command;
import by.htp.library.service.InitializationService;
import by.htp.library.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static by.htp.library.util.constant.CommandConstant.ERROR_FOR_DATABASE_INITIALIZATION;
import static by.htp.library.util.constant.CommandConstant.SUCCESSFUL_MESSAGE_FOR_DATABASE_INITIALIZATION;

@Component
public class
InitializationSource implements Command {
    private static final Logger log = Logger.getLogger(InitializationSource.class);

    @Autowired
    InitializationService initializationService;

    @Override
    public String executeCommand(String request) {
        String response;

        try {

            initializationService.initialization();
            response = SUCCESSFUL_MESSAGE_FOR_DATABASE_INITIALIZATION;
            log.debug(SUCCESSFUL_MESSAGE_FOR_DATABASE_INITIALIZATION);

        } catch (ServiceException e) {
            response = ERROR_FOR_DATABASE_INITIALIZATION;
            log.error(e);
        }

        return response;
    }

}
