package by.htp.library.controller.command.impl;

import by.htp.library.controller.command.Command;
import by.htp.library.service.UserService;
import by.htp.library.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static by.htp.library.util.constant.CommandConstant.ERROR_FOR_SIGN_UP;
import static by.htp.library.util.constant.CommandConstant.REQUEST_SPLITER;
import static by.htp.library.util.constant.CommandConstant.SUCCESSFUL_MESSAGE_FOR_SIGN_UP;

@Component
public class SignUp implements Command {
    private static final Logger log = Logger.getLogger(SignUp.class);
    @Autowired
    UserService userService;


    @Override
    public String executeCommand(String request) {
        String[] parameter = request.split(REQUEST_SPLITER);
        String login = parameter[1];
        String password = parameter[2];
        String response;

        try {
            userService.signUp(login, password);
            response = SUCCESSFUL_MESSAGE_FOR_SIGN_UP + login;
            log.debug(SUCCESSFUL_MESSAGE_FOR_SIGN_UP + login);
        } catch (ServiceException e) {
            response = ERROR_FOR_SIGN_UP;
            log.error(e);
        }

        return response;
    }

}
