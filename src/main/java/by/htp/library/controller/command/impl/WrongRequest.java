package by.htp.library.controller.command.impl;

import by.htp.library.controller.command.Command;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import static by.htp.library.util.constant.CommandConstant.WRONG_REQUEST;

@Component
public class WrongRequest implements Command {
    private static final Logger log = Logger.getLogger(AddEditBook.class);

    @Override
    public String executeCommand(String request) {
        log.debug(WRONG_REQUEST);
        return WRONG_REQUEST;
    }

}
