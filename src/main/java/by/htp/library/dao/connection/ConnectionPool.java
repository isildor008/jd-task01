package by.htp.library.dao.connection;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import by.htp.library.util.dbconfiguration.DBParameter;
import by.htp.library.dao.exception.ConnectionPoolException;
import by.htp.library.dao.exception.DAOException;
import org.apache.log4j.Logger;

import static by.htp.library.util.constant.DAOError.*;
import static by.htp.library.util.constant.DBConstant.POOL_SIZE;
import static by.htp.library.util.constant.DBConstant.RESOURCES_FILE_NAME;

public final class ConnectionPool implements Closeable {
    private static final Logger log = Logger.getLogger(ConnectionPool.class);
    private static final ResourceBundle bundle = ResourceBundle.getBundle(RESOURCES_FILE_NAME);
    private static final ConnectionPool instance = new ConnectionPool();
    private BlockingQueue<Connection> freeConnection;
    private BlockingQueue<Connection> busyConnection;

    private int poolSize;
    private String driver;
    private String user;
    private String password;
    private String url;

    private ConnectionPool() {
        this.driver = bundle.getString(DBParameter.DB_DRIVER);
        this.user = bundle.getString(DBParameter.DB_USER);
        this.password = bundle.getString(DBParameter.DB_PASSWORD);
        this.url = bundle.getString(DBParameter.DB_URL);

        try {
            this.poolSize = Integer.parseInt(bundle.getString(DBParameter.DB_POOLSIZE));
        } catch (NumberFormatException e) {
            this.poolSize = POOL_SIZE;
            log.error(e);
        }
    }

    public void init() throws ConnectionPoolException {
        freeConnection = new ArrayBlockingQueue<>(poolSize);
        busyConnection = new ArrayBlockingQueue<>(poolSize);

        try {
            Class.forName(driver);
            for (int i = 0; i < poolSize; i++) {
                freeConnection.add(DriverManager.getConnection(url, user, password));
            }
        } catch (ClassNotFoundException e) {
            log.error(ERROR_FIND_DATABASE, e);
            throw new ConnectionPoolException(ERROR_FIND_DATABASE, e);

        } catch (SQLException e) {
            log.error(ERROR_SQL_CONNECTION_POOL, e);
            throw new ConnectionPoolException(ERROR_SQL_CONNECTION_POOL, e);
        }

    }

    public Connection take() throws ConnectionPoolException {
        Connection connection;
        try {
            connection = freeConnection.take();
            busyConnection.put(connection);
        } catch (InterruptedException e) {
            log.error(ERROR_CONNECTION_DATA_SOURCE, e);
            throw new ConnectionPoolException(ERROR_CONNECTION_DATA_SOURCE, e);
        }
        return connection;
    }

    public void free(Connection connection) throws InterruptedException, DAOException {
        if (connection == null) {
            log.error(ERROR_NULL_CONNECTION);
            throw new DAOException(ERROR_NULL_CONNECTION);
        }


        busyConnection.remove(connection);
        freeConnection.put(connection);
    }

    public static ConnectionPool getInstance() {
        return instance;
    }

    @Override
    public void close() throws IOException {
        List<Connection> listConnection = new ArrayList<Connection>();
        listConnection.addAll(this.busyConnection);
        listConnection.addAll(this.freeConnection);

        for (Connection connection : listConnection) {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error(e);

            }
        }
    }

    public void closeConnection(Connection con, Statement st, PreparedStatement preSt, ResultSet rs) {
        if (con != null) {
            try {
                free(con);
            } catch (InterruptedException | DAOException e) {
                log.error(e);
            }
        }

        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                log.error(e);
            }
        }

        if (preSt != null) {
            try {
                preSt.close();
            } catch (SQLException e) {
                log.error(e);
            }
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                log.error(e);
            }
        }
    }


    public void closeConnection(Connection con, PreparedStatement preSt) {
        if (con != null) {
            try {
                free(con);
            } catch (InterruptedException | DAOException e) {
                log.error(e);
            }
        }

        if (preSt != null) {
            try {
                preSt.close();
            } catch (SQLException e) {
                log.error(e);
            }
        }
    }

    public void closeConnection(Connection con, ResultSet rs) {
        if (con != null) {
            try {
                free(con);
            } catch (InterruptedException | DAOException e) {
                log.error(e);
            }
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                log.error(e);
            }
        }
    }

    public void closeConnection(Connection con, Statement st, PreparedStatement preSt) {
        if (con != null) {
            try {
                free(con);
            } catch (InterruptedException | DAOException e) {
                log.error(e);
            }
        }

        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                log.error(e);
            }
        }

        if (preSt != null) {
            try {
                preSt.close();
            } catch (SQLException e) {
                log.error(e);
            }
        }

    }

    public void closeConnection(Connection con, PreparedStatement preSt, ResultSet rs) {
        if (con != null) {
            try {
                free(con);
            } catch (InterruptedException | DAOException e) {
                log.error(e);
            }
        }

        if (preSt != null) {
            try {
                preSt.close();
            } catch (SQLException e) {
                log.error(e);
            }
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                log.error(e);
            }
        }
    }

    public void closeConnection(Connection con, Statement st, ResultSet rs) {
        if (con != null) {
            try {
                free(con);
            } catch (InterruptedException | DAOException e) {
                log.error(e);
            }
        }

        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                log.error(e);
            }
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                log.error(e);
            }
        }
    }

}
