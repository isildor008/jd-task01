package by.htp.library.dao.exception;

import by.htp.library.dao.connection.ConnectionPool;
import org.apache.log4j.Logger;

public class ConnectionPoolException extends Exception {
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ConnectionPool.class);

    public ConnectionPoolException(String message, Exception e) {
        super(message, e);
        log.error(message, e);
    }

}
