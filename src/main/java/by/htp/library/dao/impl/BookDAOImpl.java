package by.htp.library.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.htp.library.controller.command.impl.AddEditBook;
import by.htp.library.entuty.Book;
import by.htp.library.dao.BookDAO;
import by.htp.library.util.constant.ColumnLabel;
import by.htp.library.util.constant.SQLCommand;
import by.htp.library.dao.connection.ConnectionPool;
import by.htp.library.dao.exception.ConnectionPoolException;
import by.htp.library.dao.exception.DAOException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import static by.htp.library.util.constant.DAOError.*;

@Repository
public class BookDAOImpl implements BookDAO {
    private static final Logger log = Logger.getLogger(AddEditBook.class);

    @Override
    public void addNewBook(String title, String author, String genre, String year, int quantity) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(SQLCommand.INSERT_BOOK);
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, author);
            preparedStatement.setString(3, genre);
            preparedStatement.setString(4, year);
            preparedStatement.setInt(5, quantity);
            preparedStatement.executeUpdate();
        } catch (ConnectionPoolException e) {
            log.error(ERROR_CONNECTION_DATABASE, e);
            throw new DAOException(ERROR_CONNECTION_DATABASE, e);
        } catch (SQLException e) {
            log.error(ERROR_EXECUTING_QUERY_INSERT_BOOK, e);
            throw new DAOException(ERROR_EXECUTING_QUERY_INSERT_BOOK, e);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
    }

    @Override
    public void addEditBook(String title, String genre, String author, String year, int quantity, int idBook) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(SQLCommand.UPDATE_BOOK);
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, author);
            preparedStatement.setString(3, genre);
            preparedStatement.setString(4, year);
            preparedStatement.setInt(5, quantity);
            preparedStatement.setInt(6, idBook);
            preparedStatement.executeUpdate();
        } catch (ConnectionPoolException e) {
            log.error(ERROR_CONNECTION_TO_DATABASE, e);
            throw new DAOException(ERROR_CONNECTION_TO_DATABASE, e);
        } catch (SQLException e) {
            log.error(ERROR_EXECUTING_QUERY_UPDATE_BOOK, e);
            throw new DAOException(ERROR_EXECUTING_QUERY_UPDATE_BOOK, e);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
    }

    @Override
    public List<Book> getBookList() throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<Book> bookList;

        try {
            connection = pool.take();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQLCommand.SELECT_BOOK);

            bookList = new ArrayList<Book>();
            Book book;

            while (resultSet.next()) {
                book = new Book();
                book.setId(resultSet.getInt(ColumnLabel.BOOK_ID));
                book.setTitle(resultSet.getString(ColumnLabel.BOOK_TITLE));
                book.setAuthor(resultSet.getString(ColumnLabel.BOOK_AUTHOR));
                book.setGenre(resultSet.getString(ColumnLabel.BOOK_GENRE));
                book.setYear(resultSet.getString(ColumnLabel.BOOK_YEAR));
                book.setQuantity(resultSet.getInt(ColumnLabel.BOOK_QUANTITY));
                book.setStatus(resultSet.getBoolean(ColumnLabel.BOOK_STATUS));
                bookList.add(book);
            }

        } catch (ConnectionPoolException e) {
            log.error(ERROR_CONNECTION_DATABASE, e);
            throw new DAOException(ERROR_CONNECTION_DATABASE, e);
        } catch (SQLException e) {
            log.error(ERROR_EXECUTING_QUERY_SELECT_BOOK, e);
            throw new DAOException(ERROR_EXECUTING_QUERY_SELECT_BOOK, e);
        } finally {
            pool.closeConnection(connection, statement, resultSet);
        }

        return bookList;
    }


}
