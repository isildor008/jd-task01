package by.htp.library.dao.impl;

import java.io.IOException;

import by.htp.library.dao.InitializationDAO;
import by.htp.library.dao.connection.ConnectionPool;
import by.htp.library.dao.exception.ConnectionPoolException;
import by.htp.library.dao.exception.DAOException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import static by.htp.library.util.constant.DAOError.ERROR_CLOSE_ALL_CONNECTION;
import static by.htp.library.util.constant.DAOError.ERROR_INITIALIZATION_DATABASE;

@Repository
public class InitializationDAOImpl implements InitializationDAO {
    private static final Logger log = Logger.getLogger(ConnectionPool.class);

    @Override
    public void initialization() throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try {
            connectionPool.init();
        } catch (ConnectionPoolException e) {
            log.error(ERROR_INITIALIZATION_DATABASE, e);

            throw new DAOException(ERROR_INITIALIZATION_DATABASE, e);
        }
    }

    @Override
    public void destroy() throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try {
            connectionPool.close();
        } catch (IOException e) {
            log.error(ERROR_CLOSE_ALL_CONNECTION, e);
            throw new DAOException(ERROR_CLOSE_ALL_CONNECTION, e);
        }
    }

}
