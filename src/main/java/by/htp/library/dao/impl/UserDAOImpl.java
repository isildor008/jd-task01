package by.htp.library.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import by.htp.library.entuty.User;
import by.htp.library.util.constant.ColumnLabel;
import by.htp.library.util.constant.SQLCommand;
import by.htp.library.dao.UserDAO;
import by.htp.library.dao.connection.ConnectionPool;
import by.htp.library.dao.exception.ConnectionPoolException;
import by.htp.library.dao.exception.DAOException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import static by.htp.library.util.constant.DAOError.ERROR_CONNECTION_DATABASE;
import static by.htp.library.util.constant.DAOError.ERROR_EXECUTING_QUERY_INSERT_USER;
import static by.htp.library.util.constant.DAOError.ERROR_EXECUTING_QUERY_SELECT_BY_LOGIN;

@Repository
public class UserDAOImpl implements UserDAO {
    private static final Logger log = Logger.getLogger(ConnectionPool.class);

    @Override
    public User signIn(String login, int password) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        User user = null;

        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(SQLCommand.SELECT_USER_BY_LOGIN_PASSWORD);
            preparedStatement.setString(1, login);
            preparedStatement.setInt(2, password);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt(ColumnLabel.USER_ID));
                user.setLogin(resultSet.getString(ColumnLabel.USER_LOGIN));
                user.setPassword(resultSet.getInt(ColumnLabel.USER_PASSWORD));
            }
        } catch (ConnectionPoolException e) {
            log.error(ERROR_CONNECTION_DATABASE, e);
            throw new DAOException(ERROR_CONNECTION_DATABASE, e);
        } catch (SQLException e) {
            log.error(ERROR_EXECUTING_QUERY_SELECT_BY_LOGIN, e);
            throw new DAOException(ERROR_EXECUTING_QUERY_SELECT_BY_LOGIN, e);
        } finally {
            pool.closeConnection(connection, preparedStatement, resultSet);
        }

        return user;

    }

    @Override
    public void signUp(String login, int password) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = pool.take();
            preparedStatement = connection.prepareStatement(SQLCommand.INSERT_USER);
            preparedStatement.setString(1, login);
            preparedStatement.setInt(2, password);
            preparedStatement.executeUpdate();
        } catch (ConnectionPoolException e) {
            log.error(ERROR_CONNECTION_DATABASE, e);
            throw new DAOException(ERROR_CONNECTION_DATABASE, e);
        } catch (SQLException e) {
            log.error(ERROR_EXECUTING_QUERY_INSERT_USER, e);
            throw new DAOException(ERROR_EXECUTING_QUERY_INSERT_USER, e);
        } finally {
            pool.closeConnection(connection, preparedStatement);
        }
    }

}
