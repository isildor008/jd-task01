package by.htp.library.service.impl;

import java.util.IllegalFormatException;
import java.util.List;

import by.htp.library.dao.connection.ConnectionPool;
import by.htp.library.entuty.Book;
import by.htp.library.dao.BookDAO;
import by.htp.library.dao.exception.DAOException;
import by.htp.library.service.BookService;
import by.htp.library.service.exception.ServiceException;
import by.htp.library.service.validation.ValidationData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static by.htp.library.util.constant.ServiceError.*;

@Service
public class BookServiceImpl implements BookService {
    private static final Logger log = Logger.getLogger(ConnectionPool.class);
    @Autowired
    BookDAO bookDAO;

    @Override
    public void addNewBook(String title, String genre, String author, String year, String quantityStr) throws ServiceException {
        if (!ValidationData.validBook(title, genre, author, year, quantityStr)) {
            log.error(ERROR_INCORET_DATA_ABUT_BOOK);
            throw new ServiceException(ERROR_INCORET_DATA_ABUT_BOOK);

        }

        int quantity;
        try {
            quantity = Integer.parseInt(quantityStr);
        } catch (IllegalFormatException e) {
            log.error(ERROR_YEAR_FORMAT, e);
            throw new ServiceException(ERROR_YEAR_FORMAT);
        }

        try {
            bookDAO.addNewBook(title, author, genre, year, quantity);
        } catch (DAOException e) {
            log.error(ERROR_ADD_BOOK_TO_LIBRARY, e);
            throw new ServiceException(ERROR_ADD_BOOK_TO_LIBRARY);
        }

    }

    @Override
    public void addEditBook(String title, String genre, String author, String year, String quantityStr, String idBookStr) throws ServiceException {
        if (!ValidationData.validBook(title, genre, author, year, quantityStr, idBookStr)) {
            log.error(ERROR_INCORRECT_DATA_ABUT_BOOK);
            throw new ServiceException(ERROR_INCORRECT_DATA_ABUT_BOOK);
        }

        int idBook = Integer.parseInt(idBookStr);
        int quantity = Integer.parseInt(quantityStr);


        try {
            bookDAO.addEditBook(title, genre, author, year, quantity, idBook);
        } catch (DAOException e) {
            log.error(ERROR_EDIT_BOOK, e);
            throw new ServiceException(ERROR_EDIT_BOOK);
        }
    }

    @Override
    public List<Book> getBookList() throws ServiceException {

        List<Book> bookList;

        try {
            bookList = bookDAO.getBookList();
        } catch (DAOException e) {
            log.error(e);
            throw new ServiceException(e);
        }

        if (bookList == null) {
            log.error(ERROR_BOOK_LIST_NOT_FOND);
            throw new ServiceException(ERROR_BOOK_LIST_NOT_FOND);
        }

        return bookList;
    }

}
