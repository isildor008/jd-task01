package by.htp.library.service.impl;

import by.htp.library.dao.InitializationDAO;
import by.htp.library.dao.connection.ConnectionPool;
import by.htp.library.dao.exception.DAOException;
import by.htp.library.service.InitializationService;
import by.htp.library.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static by.htp.library.util.constant.ServiceError.ERROR_DESRTROY;
import static by.htp.library.util.constant.ServiceError.ERROR_INITIALIZATION;

@Service
public class InitializationServiceImpl implements InitializationService {
    private static final Logger log = Logger.getLogger(ConnectionPool.class);
    @Autowired
    InitializationDAO initializationDAO;

    @Override
    public void initialization() throws ServiceException {

        try {
            initializationDAO.initialization();
        } catch (DAOException e) {
            log.error(ERROR_INITIALIZATION, e);
            throw new ServiceException(ERROR_INITIALIZATION, e);
        }
    }

    @Override
    public void destroy() throws ServiceException {
        try {
            initializationDAO.destroy();
        } catch (DAOException e) {
            log.error(ERROR_DESRTROY, e);
            throw new ServiceException(ERROR_DESRTROY, e);
        }
    }

}
