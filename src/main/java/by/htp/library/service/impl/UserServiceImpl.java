package by.htp.library.service.impl;

import by.htp.library.dao.connection.ConnectionPool;
import by.htp.library.entuty.User;
import by.htp.library.dao.UserDAO;
import by.htp.library.dao.exception.DAOException;
import by.htp.library.service.UserService;
import by.htp.library.service.exception.ServiceException;
import by.htp.library.service.validation.ValidationData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static by.htp.library.util.constant.CommandConstant.ERROR_FOR_SIGN_UP;
import static by.htp.library.util.constant.ServiceError.ERROR_LOGIN_OR_PASSWORD;
import static by.htp.library.util.constant.ServiceError.ERROR_USER_NOT_FOUND;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger log = Logger.getLogger(ConnectionPool.class);
    @Autowired
    UserDAO userDAO;

    @Override
    public void signIn(String login, String password) throws ServiceException {
        if (!ValidationData.validUser(login, password)) {
            log.error(ERROR_LOGIN_OR_PASSWORD);
            throw new ServiceException(ERROR_LOGIN_OR_PASSWORD);
        }

        try {
            User user = userDAO.signIn(login, password.hashCode());
            if (user == null) {
                log.error(ERROR_USER_NOT_FOUND);
                throw new ServiceException(ERROR_USER_NOT_FOUND);
            }
        } catch (DAOException e) {
            throw new ServiceException(ERROR_FOR_SIGN_UP, e);
        }
    }

    @Override
    public void signUp(String login, String password) throws ServiceException {
        if (!ValidationData.validUser(login, password)) {
            log.error(ERROR_LOGIN_OR_PASSWORD);
            throw new ServiceException(ERROR_LOGIN_OR_PASSWORD);
        }
        try {
            userDAO.signUp(login, password.hashCode());
        } catch (DAOException e) {
            log.error(ERROR_FOR_SIGN_UP);
            throw new ServiceException(ERROR_FOR_SIGN_UP, e);
        }
    }

}
