package by.htp.library.util.constant;

/**
 * Created by Dmitry on 10.08.2017.
 */
public class CommandConstant {
    public static final String RESPONSE_SPLITER=" ";
    public static final String REQUEST_SPLITER=" ";

    //Susses  messages
    public static final String SUCCESSFUL_MESSAGE_FOR_ADD_BOOK ="Book successfully added";
    public static final String SUCCESSFUL_MESSAGE_FOR_EDIT_BOOK ="Book successfully edit";
    public static final String SUCCESSFUL_MESSAGE_FOR_DESTROY_DATABASE="Database has been destroyed";
    public static final String SUCCESSFUL_MESSAGE_FOR_BOOK_LIST="List of books received";
    public static final String SUCCESSFUL_MESSAGE_FOR_DATABASE_INITIALIZATION="Database has been initialized";
    public static final String SUCCESSFUL_MESSAGE_FOR_WELCOME="Welcome";
    public static final String SUCCESSFUL_MESSAGE_FOR_SIGN_UP="User was registrated ";

    //Error messages
    public static final String ERROR_EDIT_FOR_BOOK="Error editing book";
    public static final String ERROR_ADD_FOR_BOOK="Error adding book";
    public static final String ERROR_FOR_DATABASE_DESTROY="Database has not been destroyed";
    public static final String ERROR_FOR_BOOK_LIST="Error getting list of books";
    public static final String ERROR_FOR_DATABASE_INITIALIZATION="Database has not been initialized";
    public static final String ERROR_FOR_SIGN_IN="Sign in error";
    public static final String ERROR_FOR_SIGN_UP="Sign up error";
    public static final String WRONG_REQUEST="Wrong request!";








}
