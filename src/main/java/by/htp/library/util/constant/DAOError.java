package by.htp.library.util.constant;

/**
 * Created by Dmitry on 11.08.2017.
 */
public class DAOError {

    public static final String  ERROR_FIND_DATABASE="Can't find database driver class";
    public static final String  ERROR_SQL_CONNECTION_POOL="SQLException in ConnectionPool";
    public static final String  ERROR_CONNECTION_DATA_SOURCE="Error connecting to the data source";
    public static final String  ERROR_NULL_CONNECTION="Connection is null";
    public static final String  ERROR_CONNECTION_DATABASE="There was a problem connecting to the database";
    public static final String  ERROR_EXECUTING_QUERY_INSERT_BOOK="Error executing the query 'insert_book'";
    public static final String  ERROR_CONNECTION_TO_DATABASE="There was a problem connecting to the database";
    public static final String  ERROR_EXECUTING_QUERY_UPDATE_BOOK="Error executing the query 'update_book'";
    public static final String  ERROR_EXECUTING_QUERY_SELECT_BOOK="Error executing the query 'select_book'";
    public static final String  ERROR_INITIALIZATION_DATABASE="There was a problem initialization database";
    public static final String  ERROR_CLOSE_ALL_CONNECTION="Failure to close all connections";
    public static final String  ERROR_EXECUTING_QUERY_SELECT_BY_LOGIN="Error executing the query 'select_user_id_by_login_password'";
    public static final String  ERROR_EXECUTING_QUERY_INSERT_USER="Error executing the query 'insert_user'";
}
