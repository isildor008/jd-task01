package by.htp.library.util.constant;

/**
 * Created by Dmitry on 11.08.2017.
 */
public class DBConstant {
    public static final String RESOURCES_FILE_NAME="dbconfig";
    public static final int POOL_SIZE=2;
}
