package by.htp.library.util.constant;

/**
 * Created by Dmitry on 11.08.2017.
 */
public class ServiceError {
    public static final String ERROR_INCORET_DATA_ABUT_BOOK="Incorrect data about book";
    public static final String ERROR_YEAR_FORMAT="Year format exception";
    public static final String ERROR_ADD_BOOK_TO_LIBRARY="Error adding a book to the library";
    public static final String ERROR_INCORRECT_DATA_ABUT_BOOK="Incorrect data about book";
    public static final String ERROR_EDIT_BOOK="Error editing book";
    public static final String ERROR_BOOK_LIST_NOT_FOND="BookList not found";
    public static final String ERROR_INITIALIZATION="Error initialization";
    public static final String ERROR_DESRTROY="Error destroy";
    public static final String ERROR_LOGIN_OR_PASSWORD="Iccorrent user's login or password";
    public static final String ERROR_USER_NOT_FOUND="User is not found";
}
