package by.htp.library.util.constant;

/**
 * Created by Dmitry on 11.08.2017.
 */
public class StartPointMessage {
    public static final String INITIALIZATION_SOURCE = "Initialization source";
    public static final String DESTROY_SOURCE = "DESTROY SOURCE ";

}
