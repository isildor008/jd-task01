package by.htp.library.service.impl;

import java.io.IOException;

import by.htp.library.service.exception.ServiceException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import by.htp.library.dao.connection.ConnectionPool;
import by.htp.library.dao.exception.ConnectionPoolException;
import by.htp.library.service.BookService;


import static by.htp.library.util.constant.TestConstant.ERROR_INCORRECT_DATA_ABUT_BOOK;

public class TestBookServiceImpl {



	@BeforeClass
	public static void initSource() throws ConnectionPoolException{
		ConnectionPool connectionPool = ConnectionPool.getInstance();
		connectionPool.init();
	}

	@AfterClass
	public static void destroySource() throws ConnectionPoolException, IOException{
		ConnectionPool connectionPool = ConnectionPool.getInstance();
		connectionPool.close();
	}

	@Test  (expected = ServiceException.class)
	public void testAddNewBook() throws ServiceException{
		BookService bookService=new BookServiceImpl();
		bookService.addNewBook(null, null, null, null, null);
	}

	@Test
	public void testAddEditBook(){
		try {
			BookService bookService=new BookServiceImpl();
			bookService.addEditBook(null, "MyAuthor", "MyGenre", "2017", "10", "1");
		} catch (ServiceException e) {
			Assert.assertEquals(ERROR_INCORRECT_DATA_ABUT_BOOK, e.getMessage());
		}
	}
	
}
