package by.htp.library.service.impl;

import org.junit.Test;

import by.htp.library.service.UserService;
import by.htp.library.service.exception.ServiceException;

import static by.htp.library.util.constant.TestConstant.LOGIN_SIGN_UP;
import static by.htp.library.util.constant.TestConstant.PASSWORD;

public class TestUserServiceImpl {
	UserService userService=new UserServiceImpl();
	@Test (expected = NullPointerException.class)
	public void signUp() throws ServiceException{
		userService.signUp(LOGIN_SIGN_UP, PASSWORD);
	}
}
